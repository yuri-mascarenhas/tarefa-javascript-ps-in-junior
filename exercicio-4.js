function quest1() {
    for (i in gods) { console.log(gods[i].name, ' ', gods[i].features.length) };
}

function quest2() {
    for (i in gods) {
        console.log(gods[i].roles);
        for (role of gods[i].roles) {
            if (role == "Mid") {
                console.log(gods[i]);
            }
        }
    }
}

function quest3() {
    gods.sort(function (a, b) {
        if (a.pantheon > b.pantheon) {
            return 1;
        }
        if (a.pantheon < b.pantheon) {
            return -1;
        }
        return 0;
    });
    console.log(gods);
}

function quest4() {
    final = new Array(gods.length);
    for (i = 0; i < gods.length; i++) {
        final[i] = `${gods[i].name} (${gods[i].class})`;
    }
    console.log(final);
}

quest1();
quest2();
quest3();
quest4();