function Aluno(n1, n2, n3) {
    this.p1 = n1;
    this.p2 = n2;
    this.p3 = n3;
}

let joao = new Aluno(5, 5, 7);
mediaJoao = (joao.p1 + joao.p2 + joao.p3) / 3;
if (mediaJoao >= 6) {
    console.log("Aprovado");
} else {
    console.log("Reprovado");
}