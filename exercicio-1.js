function multiply(mat1, mat2) {
    if (mat1[0].length != mat2.length) {
        console.log("Tamanho das matrizes não é compatível.");
    } else {
        let mat1Rows = mat1.length, mat1Cols = mat1[0].length,
            mat2Rows = mat2.length, mat2Cols = mat2[0].length,
            final = new Array(mat1Rows);
        for (var r = 0; r < mat1Rows; r++) {
            final[r] = new Array(mat2Cols);
            for (var c = 0; c < mat2Cols; c++) {
                final[r][c] = 0;
                for (var i = 0; i < mat1Cols; i++) {
                    final[r][c] += mat1[r][i] * mat2[i][c];
                }
            }
        }
        return final;
    }
}

let teste1A =
    [
        [2, -1],
        [2, 0]
    ];
let teste1B =
    [
        [2, 3],
        [-2, 1]
    ];

let testeA = multiply(teste1A, teste1B);
console.log(testeA);